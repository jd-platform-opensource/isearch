FROM ubuntu:18.04
RUN echo "deb http://dk.archive.ubuntu.com/ubuntu/ xenial main" >> /etc/apt/sources.list &&\
    echo "deb http://dk.archive.ubuntu.com/ubuntu/ xenial universe" >> /etc/apt/sources.list &&\
    apt update &&\
    apt install -y git wget curl psmisc zlib1g-dev libsnappy-dev openssl libssl-dev autoconf automake libtool curl make unzip liblapack-dev libopenblas-dev jq
RUN ln -s /usr/lib/x86_64-linux-gnu/liblapack.a /lib64/ &&\
    ln -s /usr/lib/x86_64-linux-gnu/libopenblas.so /lib64/ &&\
    apt install gcc-4.9 g++-4.9 -y &&\
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 20 &&\
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.9 20 &&\
    cd /usr/local &&\
    wget --no-check-certificate https://cmake.org/files/v3.21/cmake-3.21.2.tar.gz &&\
    tar zxvf cmake-3.21.2.tar.gz &&\
    cd cmake-3.21.2 &&\
    ./bootstrap  &&\
    make -j8 && make install &&\
    cd /usr/local &&\
    git clone https://github.com/gflags/gflags.git &&\
    cd gflags &&\
    git checkout -b 2.2 v2.2.2 &&\
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON -DGFLAGS_NAMESPACE=google -G "Unix Makefiles" . &&    make && make install &&\
    cd /usr/local &&\
    git clone https://github.com/facebook/rocksdb.git  &&\
    cd rocksdb/ &&\
    git checkout -b 6.6.0 ad528fe5ca08dafff47d79c85abbf3e1fbb21568 &&    make -j8 shared_lib  &&\
    cp /usr/local/rocksdb/librocksdb.so.6.6.0  /usr/lib/librocksdb.so.6 &&\
    cd /usr/local &&\
    wget http://storage.jd.com/lbs-search-acc/protobuf-3.12.2.zip &&\
    unzip protobuf-3.12.2.zip  &&\
    cd protobuf-3.12.2 &&\
    ./autogen.sh  &&\
    ./configure  &&\
    make -j8 && make install &&\
    ln -s  /usr/local/lib/libprotobuf.so.23.0.2 /lib64/ &&\
    ln -s  /usr/local/lib/libprotoc.so.23.0.2 /lib64/ &&\
    cd /usr/local &&\
    wget http://storage.jd.com/lbs-search-acc/faiss-main.zip &&\
    unzip faiss-main.zip  &&\
    cd faiss-main &&\
    cmake -B build . -DFAISS_ENABLE_GPU=OFF -DFAISS_ENABLE_PYTHON=OFF -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=ON -DBLA_VENDOR=Generic -DBLAS_LIBRARIES=/lib64/libopenblas.so -DLAPACK_LIBRARIES=/lib64/liblapack.a &&\
    cd build/ && make -j8 && make install &&\
    /bin/rm -rf /usr/local/protobuf-3.12.2 &&\
    /bin/rm -rf /usr/local/rocksdb &&\
    /bin/rm -rf /usr/local/cmake-3.21.2