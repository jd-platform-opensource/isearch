#ifndef __SA_MESSAGE_POOL_H_
#define __SA_MESSAGE_POOL_H_
#include <stdint.h>
#include <set>



using namespace std;

class CSearchAgentMessage;
class CSearchAgentCore;
class ConnectionInterface;


struct MsgNode
{
    uint64_t key;
    uint32_t msg_id;
    void *data;
    MsgNode()
    {
    }
    MsgNode(const MsgNode &node)
    {
        this->key = node.key;
        this->msg_id = node.msg_id;
        this->data = node.data;
    }
    bool operator<(const MsgNode &node) const
    {
        if (this->key != node.key)
        {
            return this->key < node.key;
        }
        return msg_id < node.msg_id;
    }
};


class CSearchAgentMessagePool
{
public:
    void InitMessage();
    CSearchAgentMessage *GetMessage(ConnectionInterface *pConnection,
                                    CSearchAgentCore *pCore,
                                    bool isRequest);
    void ReclaimTimeoutMessage(uint64_t &iNewTimeout,
                               CSearchAgentCore *pCore);

    void EnMessageTreeSet(CSearchAgentMessage *pMessage, uint64_t iTimeout);
    void DeMessageTreeSet(CSearchAgentMessage* pMessage);


private:
    uint32_t m_iMessageId;
    uint64_t m_iFragId;
    set<MsgNode> m_oMessageSet;
private:
    void ReclaimMessage(CSearchAgentMessage *pMessage,
                        CSearchAgentCore *pCore);
    void ReclaimSendingMessage(CSearchAgentMessage *pMessage,
                               CSearchAgentCore *pCore);
};

#endif
