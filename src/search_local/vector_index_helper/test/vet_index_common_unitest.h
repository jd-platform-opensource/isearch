#ifndef VET_INDEX_COMMON_UNITEST_H_
#define VET_INDEX_COMMON_UNITEST_H_

#include "unitest_comm.h"
#include "../include/comm/vet_index_common.h"

UNITEST_NAMESPACE_BEGIN

class VetIdxComTest : public testing::Test{
protected:
    UniqueIndexFlag o_unique_index_flag;
    std::map<UniqueIndexFlag,int> o_unique_idx;
    VetReqCnt o_vet_req;
};

TEST_F(VetIdxComTest, UniqueIndexTest){
    this->o_unique_index_flag.i_appid_ = 10010;
    this->o_unique_index_flag.i_field_id_ = 1;
    this->o_unique_index_flag.i_index_typeid_ = 1;
    o_unique_idx.insert(std::make_pair(o_unique_index_flag , 0));
    o_unique_idx.insert(std::make_pair(o_unique_index_flag , 1));
    EXPECT_TRUE(o_unique_idx.size() == 1);
    EXPECT_TRUE(o_unique_idx[o_unique_index_flag] == 0);

    this->o_unique_index_flag.i_appid_ = 10001;
    this->o_unique_index_flag.i_field_id_ = 1;
    this->o_unique_index_flag.i_index_typeid_ = 1;
    o_unique_idx.insert(std::make_pair(o_unique_index_flag , 1));
    EXPECT_TRUE(o_unique_idx.size() == 2);
    EXPECT_TRUE(o_unique_idx[o_unique_index_flag] == 1);

    this->o_unique_index_flag.i_appid_ = 10010;
    this->o_unique_index_flag.i_field_id_ = 2;
    this->o_unique_index_flag.i_index_typeid_ = 1;
    o_unique_idx.insert(std::make_pair(o_unique_index_flag , 2));
    EXPECT_TRUE(o_unique_idx.size() == 3);
    EXPECT_TRUE(o_unique_idx[o_unique_index_flag] == 2);

    this->o_unique_index_flag.i_appid_ = 10010;
    this->o_unique_index_flag.i_field_id_ = 1;
    this->o_unique_index_flag.i_index_typeid_ = 2;
    o_unique_idx.insert(std::make_pair(o_unique_index_flag , 3));
    EXPECT_TRUE(o_unique_idx.size() == 4);
    EXPECT_TRUE(o_unique_idx[o_unique_index_flag] == 3);
};

TEST_F(VetIdxComTest , VetReqCntTest){
    float p_test[] = {0.001,0.002,0.003,0.004};
    for (size_t i = 0; i < sizeof(p_test) / sizeof(float); i++)
    {
        o_vet_req.o_vet_data_.emplace_back(p_test[i]);
    }
    
    EXPECT_TRUE(o_vet_req.CheckIsValid(4));
    EXPECT_FALSE(o_vet_req.CheckIsValid(3));
};

UNITEST_NAMESPACE_END
#endif
