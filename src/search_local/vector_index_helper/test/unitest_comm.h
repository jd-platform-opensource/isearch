#ifndef UNITEST_COMM_H_
#define UNITEST_COMM_H_

#include <malloc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define UNITEST_NAMESPACE_BEGIN     namespace testing{
#define UNITEST_NAMESPACE_END       }

#endif