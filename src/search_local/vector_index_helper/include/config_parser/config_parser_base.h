#ifndef CONFIG_PARSER_BASE_H_
#define CONFIG_PARSER_BASE_H_

#include "vet_index_common.h"

VET_INDEX_NAMESPACE_BEGIN

struct ConfigParserBase
{
    /** parse index type from "../conf" directory
    */
    virtual ~ConfigParserBase() {};
    virtual bool ParseConfig() = 0;
};

VET_INDEX_NAMESPACE_END

#endif
