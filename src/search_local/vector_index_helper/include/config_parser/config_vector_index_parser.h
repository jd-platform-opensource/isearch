#ifndef CONFIG_VECTOR_INDEX_PARSER_H_
#define CONFIG_VECTOR_INDEX_PARSER_H_

#include "vet_index_common.h"
#include "config_parser_base.h"

VET_INDEX_NAMESPACE_BEGIN

struct VectorIndexContext : public ConfigParserBase
{
    std::string s_service_name_;
    std::string s_pid_file_;
    std::string s_log_dir_;
    uint16_t  ui_log_level_;
    bool b_daemon_;
    std::string s_listen_addr_;
    uint32_t ui_listen_port_;
    std::string s_local_socket_;
    std::string s_rocksdb_dir_;


    bool ParseConfig() override;
};

VET_INDEX_NAMESPACE_END

#endif

