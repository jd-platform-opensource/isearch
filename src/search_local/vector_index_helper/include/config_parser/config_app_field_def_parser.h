#ifndef APP_FIELD_DEFINE_PARSER_H_
#define APP_FIELD_DEFINE_PARSER_H_

#include "vet_index_common.h"
#include "config_parser_base.h"

VET_INDEX_NAMESPACE_BEGIN

struct AppFieldContext : public ConfigParserBase
{
    struct VectorIndexCont
    {
        uint16_t ui_dim_;
        std::vector<std::string> index_type_str_array_;
        std::vector<std::string> index_dir_array_;
        uint16_t ui_search_index_type_;
        std::string s_metric_type_;

        bool IsValid() {
            int i_min_size = MIN(index_type_str_array_.size(),
                                index_dir_array_.size());
            return (ui_search_index_type_ < i_min_size);
        };
    };

    using AppidType = uint16_t;
    using FieldIdType = uint16_t;

    using FieldIdVetMap = std::map<FieldIdType , VectorIndexCont>;
    using FieldIdVetIterator = FieldIdVetMap::iterator;

    using AppFieldTable = std::map<AppidType , FieldIdVetMap>;
    using AppFileldTableIterator = AppFieldTable::iterator;

    AppFieldTable o_app_field_table_;

    bool ParseConfig() override;

    bool ValidityCheck(AppidType ui_appid , FieldIdType ui_field_id);

    const VectorIndexCont& GetValue(AppidType ui_appid , FieldIdType ui_field_id) {
        return o_app_field_table_[ui_appid][ui_field_id];
    };
};

VET_INDEX_NAMESPACE_END
#endif