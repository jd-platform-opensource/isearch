#ifndef VET_SYSTEM_STATUS_H_
#define VET_SYSTEM_STATUS_H_

#include "vet_index_common.h"
#include "singleton.h"
#include "noncopyable.h"
#include "rocksdb_basic_operator.h"

VET_INDEX_NAMESPACE_BEGIN

class SystemState : private noncopyable
{
public:
    SystemState() 
        : s_cur_vet_id_("-1"){ };
    virtual ~SystemState() {};
public:
    static SystemState* Instance() {
        return CSingleton<SystemState>::Instance();
    };

    static void Destroy() {
        CSingleton<SystemState>::Destroy();
    };

public:
    const std::string& GetVetId() {
        return s_cur_vet_id_;
    };

    void SetVetId(const std::string& _vet_id) {
        s_cur_vet_id_ = _vet_id;
    };

    int AutoIncVetId() {
        s_cur_vet_id_ = std::to_string((int64_t)strtoll(s_cur_vet_id_.c_str(), NULL, 10) + 1);
        int i_ret =  RocksDbOperator::Instance()->ReplaceEntry(
                        VET_CURRENT_NUM_KEY,
                        s_cur_vet_id_,
                        false,
                        E_COL_FAM_META_DATA);
        return i_ret;
    };

private:
    std::string s_cur_vet_id_;
};

VET_INDEX_NAMESPACE_END
#endif