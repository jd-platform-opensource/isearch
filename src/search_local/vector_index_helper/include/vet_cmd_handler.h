#ifndef VET_CMD_HANDLER_H_
#define VET_CMD_HANDLER_H_

#include "faiss_index_factory.h"
#include "rocksdb_basic_operator.h"
#include "config_manager.h"
#include "request_base.h"
#include "task_request.h"

VET_INDEX_NAMESPACE_BEGIN

class CmdHandler : public CTaskDispatcher<CTaskRequest>
{
public:
    CmdHandler(CPollThread* p_thread);
    virtual ~CmdHandler();

public:
    static std::string VetKeyFormat (
        const VetReqCnt& o_vet_req ,
        uint32_t ui_no_vet ,
        uint32_t ui_dim);
public:
    void TaskNotify(CTaskRequest* p_task_req) override;

private:
    void set_reply_info(CTaskRequest* p_task_req ,
             const VetRspCnt& o_vet_rsp_cnt);

private:
    IndexFactory* p_vet_idx_;
    RocksDbOperator* p_rocksdb_;
    AppFieldContext* p_app_field_table_;
};

VET_INDEX_NAMESPACE_END

#endif