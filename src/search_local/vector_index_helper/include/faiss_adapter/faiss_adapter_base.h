#ifndef FAISS_ADAPTER_H_
#define FAISS_ADAPTER_H_

#include "vet_index_common.h"
#include "Index.h"
#include "impl/FaissException.h"

VET_INDEX_NAMESPACE_BEGIN

class FaissAptBase{
public:
    using idx_t = int64_t;

public:
    FaissAptBase(faiss::Index* p_index , bool b_has_delete = true);
    virtual ~FaissAptBase();

public:
    /** insert vector + id into faiss specific index
     * 
     * @param p_vetcont_array   insert vector float array pointer
     * @param p_vetid_array     insert vector id int array pointer
     * @param i_num             total vector num need insert
     * @return success insert total num
    */
    virtual int InsertVet(const float* p_vetcont_array,
                           const idx_t* p_vetid_array,
                           idx_t i_num = 1);

    /** remove vector in corresponding Faiss Index by id
     * 
     * @param p_vetid_array remove vector unique id storaged in Rocksdb
     * @param i_num         total vector num need remove
     * @return success remove total num
    */
    virtual int RemoveVet(const idx_t* p_vetid_array,
                             idx_t i_num = 1);

    /** update vector in corresponding Faiss Index by same vector id
     * 
     * @param p_vetcont_array   update vector float array pointer
     * @param p_vetid_array     update vector id int array pointer
     * @param i_num             total vector num need updated
     * @return -1 : update error 
     *          0 : update success
    */
    virtual int UpdateVet(const float* p_vetcont_array,
                           const idx_t* p_vetid_array,
                           idx_t i_num = 1);

    /** search top-k vector similar with query vector
     * 
     * @param p_query_array     query vector float array pointer
     * @param i_top_k           k most similar vector
     * @param p_res_vetid_array similar vector id array
     * @param i_query_num       total vector num need search
     * @return total similar vector num
    */
    virtual int SearchVet(const float* p_query_array,
                            idx_t i_top_k,
                            idx_t* p_res_vetid_array,
                            float* p_res_vetdis_array,
                            idx_t i_query_num = 1);

    virtual int ExtractVet(idx_t i_idx_id, float* p_vetcont_array);

public:
    bool GetDeleEnableFlag() { return b_has_delete_;};

protected:
    faiss::Index* p_base_index_;
    bool b_has_delete_;
};

VET_INDEX_NAMESPACE_END

#endif