#ifndef FAISS_INDEX_IVF_CONVERTOR_H_
#define FAISS_INDEX_IVF_CONVERTOR_H_

#include "faiss_adapter_base.h"
#include "IndexIVFFlat.h"
#include "IndexIVFPQ.h"

VET_INDEX_NAMESPACE_BEGIN

class IndexIVFFlat : public FaissAptBase {
public:
    IndexIVFFlat(faiss::IndexIVFFlat* p_index);
    virtual ~IndexIVFFlat();

public:
    int UpdateVet(const float* p_vetcont_array,
                     const idx_t* p_vetid_array,
                     idx_t i_num) override;

private:
    faiss::IndexIVFFlat* p_index_ivf_flat_;
};

class IndexIVFPQ : public FaissAptBase {
public:
    IndexIVFPQ(faiss::IndexIVFPQ* p_index);
    virtual ~IndexIVFPQ();

public:
    int UpdateVet(const float* p_vetcont_array,
                     const idx_t* p_vetid_array,
                     idx_t i_num) override;

private:
    faiss::IndexIVFPQ* p_index_ivf_pq_;
};

VET_INDEX_NAMESPACE_END

#endif