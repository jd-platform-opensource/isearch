#ifndef VET_INDEX_COMMON_H_
#define VET_INDEX_COMMON_H_

#include <string>
#include <map>
#include <vector>
#include <set>
#include <assert.h>
#include <stdlib.h>
#include "memcheck.h"

#define VET_INDEX_NAMESPACE_BEGIN     namespace vectorindex{
#define VET_INDEX_NAMESPACE_END       }

#define VET_CURRENT_NUM_KEY         "current_vector_id"
#define VET_VALUE_PRECISION         7

#define MIN(a,b) ((a)<(b) ? (a) : (b))

struct UniqueIndexFlag
{
    int i_appid_;           // application unique identification
    int i_field_id_;        // fieldname unique id
    bool b_has_index_typeid_;
    int i_index_typeid_;    // vector index self positon in "indexType" in appconfig

    UniqueIndexFlag()
        : i_appid_(-1)
        , i_field_id_(-1)
        , b_has_index_typeid_(false)
        , i_index_typeid_(0)
    { }

    UniqueIndexFlag(
            int i_appid,
            int i_field_id,
            int i_index_typeid)
        : i_appid_(i_appid)
        , i_field_id_(i_field_id)
        , i_index_typeid_(i_index_typeid)
    { }

    bool operator<(const UniqueIndexFlag& o_unique_index) const {
        if (i_appid_ == o_unique_index.i_appid_) {
            if (i_field_id_ == o_unique_index.i_field_id_) {
                if (i_index_typeid_ == o_unique_index.i_index_typeid_) {
                    return false;
                } else {
                    return i_index_typeid_ < o_unique_index.i_index_typeid_;
                }
            } else {
                return i_field_id_ < o_unique_index.i_field_id_;
            }
        } else {
            return i_appid_ < o_unique_index.i_appid_;
        }
    }
};

struct VetReqCnt
{
    std::vector<float> o_vet_data_;
    uint32_t ui_top_k_;
    UniqueIndexFlag o_idx_flag_;

    bool CheckIsValid(uint32_t ui_dim){
        if (ui_dim <= 0 
        || o_vet_data_.empty()) {
            return false;
        }
        
        return (0 == (o_vet_data_.size() % ui_dim));
    };
};

struct VetRspCnt
{
    uint32_t ui_code;
    std::string s_info;
    std::vector<int64_t> vet_ids;
    std::vector<float> vet_dis;

    void operator()(const uint32_t& _code,
        const std::string& _info){
            ui_code = _code;
            s_info = _info;
    }
};

#endif