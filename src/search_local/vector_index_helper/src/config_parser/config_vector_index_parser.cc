#include "config_vector_index_parser.h"

#include <fstream>
#include <iostream>

#include "json/json.h"
#include "log.h"

VET_INDEX_NAMESPACE_BEGIN

bool VectorIndexContext::ParseConfig()
{
    const std::string s_path_const = "../conf/vector_index.conf";

    std::ifstream i_stream(s_path_const.c_str());
    if (!i_stream.is_open()) {
        log_error("load %s failed.", s_path_const.c_str());
        return false;
    }

    Json::Reader reader;
    Json::Value o_vector_index;
    if (reader.parse(i_stream, o_vector_index)) {
        if (o_vector_index.isMember("service_name") 
            && o_vector_index["service_name"].isString()) {
            s_service_name_ = o_vector_index["service_name"].asString();
        } else {
            log_error("service_name is incorrect");
            return false;
        }

        if (o_vector_index.isMember("pid_file") 
            && o_vector_index["pid_file"].isString()) {
            s_pid_file_ = o_vector_index["pid_file"].asString();
        } else {
            log_error("pid_file is incorrect");
            return false;
        }

        if (o_vector_index.isMember("log") 
            && o_vector_index["log"].isString()) {
            s_log_dir_ = o_vector_index["log"].asString();
        } else {
            log_error("log is incorrect");
            return false;
        }

        if (o_vector_index.isMember("log_level") 
            && o_vector_index["log_level"].isUInt()) {
            ui_log_level_ = o_vector_index["log_level"].asUInt();
        } else {
            log_error("log_level is incorrect");
            return false;
        }
        
        if (o_vector_index.isMember("daemon") 
            && o_vector_index["daemon"].isBool()) {
            b_daemon_ = o_vector_index["daemon"].asBool();
        } else {
            log_error("daemon is incorrect");
            return false;
        }

        if (o_vector_index.isMember("listen_addr") 
            && o_vector_index["listen_addr"].isString()) {
            s_listen_addr_ = o_vector_index["listen_addr"].asString();
        } else {
            log_error("listen_addr is incorrect");
            return false;
        }

        if (o_vector_index.isMember("listen_port") 
            && o_vector_index["listen_port"].isUInt()) {
            ui_listen_port_ = o_vector_index["listen_port"].asUInt();
        } else {
            log_error("listen_port is incorrect");
            return false;
        }

        if (o_vector_index.isMember("socket_dir") 
            && o_vector_index["socket_dir"].isString()) {
            s_local_socket_ = o_vector_index["socket_dir"].asString();
        } else {
            log_error("socket_dir is incorrect");
            return false;
        }

        if (o_vector_index.isMember("rocksdb_storage_dir") 
            && o_vector_index["rocksdb_storage_dir"].isString()) {
            s_rocksdb_dir_ = o_vector_index["rocksdb_storage_dir"].asString();
        } else {
            log_error("rocksdb_storage_dir is incorrect");
            return false;
        }
    } else {
        log_error("config file: %s has error format" , s_path_const.c_str());
        i_stream.close();
        return false;
    }

    return true;
}

VET_INDEX_NAMESPACE_END