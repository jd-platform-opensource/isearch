#include "config_manager.h"
#include "log.h"

VET_INDEX_NAMESPACE_BEGIN

ConfigManager::ConfigManager()
    : o_config_parser_map_()
{
    register_config_parser();
}

ConfigManager::~ConfigManager()
{
    std::map<ConfigFileType , ConfigParserBase*>::iterator iter = 
        o_config_parser_map_.begin();
    for (; iter != o_config_parser_map_.end(); ++iter) {
        DELETE(iter->second);
    }
    o_config_parser_map_.clear();
}

bool ConfigManager::InitConfig()
{
    std::map<ConfigFileType , ConfigParserBase*>::iterator iter = 
        o_config_parser_map_.begin();
    
    for (; iter != o_config_parser_map_.end(); ++iter) {
        if (iter->second != NULL) {
            bool b_ret = iter->second->ParseConfig();
            if (!b_ret) {
                log_error("config file No:%d parse error" , iter->first);
                return false;
            }
        } else {
            log_error("config file No:%d parser is null" , iter->first);
            return false;
        }
    }
    
    return true;
}

void ConfigManager::register_config_parser()
{
    o_config_parser_map_.insert(std::make_pair(E_CONFIG_FILE_APP_FIELD , 
        new AppFieldContext()));
    o_config_parser_map_.insert(std::make_pair(E_CONFIG_FILE_VECTOR_INDEX , 
        new VectorIndexContext()));
}

VET_INDEX_NAMESPACE_END