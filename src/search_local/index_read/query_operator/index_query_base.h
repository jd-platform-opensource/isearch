/*
 * =====================================================================================
 *
 *       Filename:  index_tbl_op.h
 *
 *    Description:  index tbl op class definition.
 *
 *        Version:  1.0
 *        Created:  19/03/2018
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *                  chenyujie, chenyujie28@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */
#ifndef INDEX_QUERY_BASE_H_
#define INDEX_QUERY_BASE_H_

#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <ctime>
#include <sstream>

#include "dtcapi.h"
#include "json/value.h"
#include "chash.h"
#include "memcheck.h"

#include "../comm.h"
#include "../search_conf.h"
#include "../search_util.h"
#include "../result_context.h"

struct TopDocInfo
{
	uint32_t appid;
	std::string doc_id;
	uint32_t doc_version;
	uint32_t created_time;
	uint32_t weight;
};

struct DocVersionInfo
{
	std::string doc_id;
	std::string content;
	uint32_t doc_version;
};

class IndexTableBase{
public:
	IndexTableBase() :server_() {};
	virtual ~IndexTableBase(){};

    // all index query common function declaration here!
public:
	virtual int InitServer(const SDTCHost& dtchost, const string& bindAddr = "127.0.0.1") = 0;

    // hanpin index query declaration
public:
    virtual int GetDocByShiftWord(FieldInfo fieldInfo, std::vector<IndexInfo>& doc_id_set, uint32_t appid) { return -1;};
	virtual int GetDocByShiftEnWord(FieldInfo fieldInfo, std::vector<IndexInfo>& doc_id_set, uint32_t appid) { return -1;};
    
    // invert index query declaration
public:
    virtual bool DocValid(uint32_t appid, string doc_id, bool &is_valid) { return false;};
	virtual bool DocValid(uint32_t appid, const std::vector<IndexInfo>& vecs, bool need_version, 
            std::map<std::string, uint32_t>& valid_version, hash_string_map& doc_content_map){ return false;};

	virtual bool TopDocValid(uint32_t appid, std::vector<TopDocInfo>& no_filter_docs, std::vector<TopDocInfo>& doc_info) { return false;};

	virtual bool GetDocInfo(uint32_t appid, const std::string& word, uint32_t field_id, std::vector<IndexInfo>& doc_info) { return false;};
	virtual bool GetTopDocInfo(uint32_t appid, std::string word, std::vector<TopDocInfo>& doc_info) { return false;};

	virtual bool GetScoreByField(uint32_t appid, std::string doc_id, std::string sort_field, uint32_t sort_type, ScoreInfo &score_info) { return false;};
	virtual bool GetDocContent(uint32_t appid, const std::vector<IndexInfo>& index_infos, hash_string_map& doc_content) { return false;};
	
	virtual int GetDocCnt(uint32_t appid) { return -1;};
	virtual bool GetContentByField(uint32_t appid, std::string doc_id, uint32_t doc_version, const std::vector<std::string>& fields, Json::Value &value){ return false;};

protected:
	DTC::Server server_;
};

class DefaultIndex : public IndexTableBase{
public:
    DefaultIndex():IndexTableBase(){ };
    virtual ~DefaultIndex(){};

public:
    virtual int InitServer(const SDTCHost& dtchost, const string& bindAddr){
        log_error("incorrect type index init");
        return -1;
    };
};
#endif