/*
 * =====================================================================================
 *
 *       Filename:  index_tbl_op.h
 *
 *    Description:  index tbl op class definition.
 *
 *        Version:  1.0
 *        Created:  19/03/2018
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *                  chenyujie, chenyujie28@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */
#ifndef HANPIN_INDEX_QUERY_H_
#define HANPIN_INDEX_QUERY_H_

#include "index_query_base.h"

class HanpinIndex : public IndexTableBase{
public:
	HanpinIndex();
	virtual~ HanpinIndex();

public:
	virtual int InitServer(const SDTCHost& dtchost, const std::string& bindAddr = "127.0.0.1");

public:
	virtual int GetDocByShiftWord(FieldInfo fieldInfo, std::vector<IndexInfo>& doc_id_set, uint32_t appid);
	virtual int GetDocByShiftEnWord(FieldInfo fieldInfo, std::vector<IndexInfo>& doc_id_set, uint32_t appid);

private:
	bool getSuggestDoc(FieldInfo& fieldInfo, uint32_t len, const IntelligentInfo &info, std::vector<IndexInfo> &doc_id_set, uint32_t appid);
	bool getSuggestDoc(uint32_t appid, int index, uint32_t len, uint32_t field, const IntelligentInfo &info, std::vector<IndexInfo> &doc_id_set);

	int shiftIntelligentInfo(IntelligentInfo &info, int len);

	bool getSuggestDocWithoutCharacter(uint32_t appid, int index, uint32_t len,  uint32_t field, const IntelligentInfo &info, std::vector<IndexInfo> &doc_id_set);
	bool getSuggestDocWithoutCharacter(FieldInfo& fieldInfo, uint32_t len, const IntelligentInfo &info, std::vector<IndexInfo> &doc_id_set ,uint32_t appid);
};

#endif