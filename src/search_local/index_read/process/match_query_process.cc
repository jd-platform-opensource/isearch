#include "match_query_process.h"
#include "math.h"
#include "../order_op.h"
#include "../valid_doc_filter.h"

#define DOC_CNT 10000

MatchQueryProcess::MatchQueryProcess(const Json::Value& value)
    : QueryProcess(value)
{ }

MatchQueryProcess::~MatchQueryProcess()
{ }

int MatchQueryProcess::ParseContent()
{
    return ParseContent(ORKEY);
}

int MatchQueryProcess::ParseContent(int logic_type)
{
    std::string s_field_name("");
    std::string s_field_value("");

    Json::Value::Members member = parse_value_.getMemberNames();
    Json::Value::Members::iterator iter = member.begin();

    if(parse_value_.size() == 0){
        log_error("MatchQueryProcess error, value is null");
        return -RT_PARSE_CONTENT_ERROR;
    }
    for(; iter != member.end(); iter++){
        s_field_name = (*iter);
        s_field_value = parse_value_[s_field_name].asString();
        FieldInfo fieldInfo;
        fieldInfo.query_type = E_INDEX_READ_MATCH;

        uint32_t segment_tag = SEGMENT_NONE;
        uint32_t uiRet = DBManager::Instance()->GetWordField(segment_tag, component_->Appid()
                , s_field_name, fieldInfo);

        std::vector<FieldInfo> fieldInfos;
        if (uiRet != 0 &&
            SEGMENT_DEFAULT == segment_tag){
            std::string split_data = SplitManager::Instance()->split(s_field_value, component_->Appid());
            log_debug("split_data: %s", split_data.c_str());

            std::vector<std::string> split_datas = splitEx(split_data, "|");
            int i_split_datas_len = split_datas.size();

            for (int index = 0; index < i_split_datas_len; index++){
                fieldInfo.word = split_datas[index];
                fieldInfos.push_back(std::move(fieldInfo));
            }
        }
        else if (uiRet != 0){
            fieldInfo.word = s_field_value;
            fieldInfos.push_back(std::move(fieldInfo));
        }else{
            log_error("field_name:[%s] error ,not in the app_field_define or segmentTag error", s_field_name.c_str());
            return -RT_PARSE_CONTENT_ERROR;
        }

        component_->AddToFieldList(logic_type, fieldInfos);
    }

    return 0;
}

int MatchQueryProcess::GetValidDoc()
{
    if (component_->GetFieldList(ORKEY).empty()){
        return -RT_GET_FIELD_ERROR;
    }
    return GetValidDoc(ORKEY , component_->GetFieldList(ORKEY)[FIRST_TEST_INDEX]);
}

int MatchQueryProcess::GetValidDoc(
    int logic_type,
    const std::vector<FieldInfo>& keys)
{
    std::vector<IndexInfo> index_info_vet;
    int iret = -1;
    uint32_t segment_tag = keys[FIRST_SPLIT_WORD_INDEX].segment_tag;

    if (SEGMENT_CHINESE == segment_tag ||
       SEGMENT_ENGLISH == segment_tag){
        iret = ValidDocFilter::Instance()->HanPinTextInvertIndexSearch(keys , index_info_vet);
    }else{
        iret = ValidDocFilter::Instance()->TextInvertIndexSearch(keys , index_info_vet);
    }

    if (iret != 0) { return iret; }
    ResultContext::Instance()->SetIndexInfos(logic_type , index_info_vet);
    return 0;
}