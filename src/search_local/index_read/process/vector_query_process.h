/*
 * =====================================================================================
 *
 *       Filename:  vector_query_process.h
 *
 *    Description:  vector_query_process class definition.
 *
 *        Version:  1.0
 *        Created:  10/11/2021
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */

#ifndef __VECTOR_QUERY_PROCESS_H__
#define __VECTOR_QUERY_PROCESS_H__

#include "query_process.h"

class VectorQueryProcess : public QueryProcess{
public:
    VectorQueryProcess(const Json::Value& value);
    virtual ~VectorQueryProcess();

public:
    virtual int ParseContent(int logic_type);
    virtual int GetValidDoc(int logic_type, const std::vector<FieldInfo>& keys);
    virtual int GetScore();
    virtual void SortScore(int& i_sequence , int& i_rank);
    void SetErrorContext(int iLine ,int iErId , const std::string& sError){
        log_error("[TypeParser]:lineNum:%d , errorcode:%d , errorInfo:%s" 
            , iLine , iErId , sError.c_str());
        m_iErrorCode_ = iErId;
    };

private:
    virtual int ParseContent();
    virtual int GetValidDoc();
    vector<double> vector_data_;
    FieldInfo field_info_;
    std::map<std::string, double> vec_dis_map_;
    int m_iErrorCode_;
};

#endif