#include "query_process_mock.h"
#include "query_process_unittest.h"
#include "correction_unittest.h"
#include "../index_sync/sync_index_timer.h"

pthread_mutex_t mutex;
SyncIndexTimer *globalSyncIndexTimer;
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}