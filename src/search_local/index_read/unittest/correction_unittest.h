#include "correction.h"
#include "gtest/gtest.h"
#include "../search_conf.h"
#include "../split_manager.h"
#include "../data_manager.h"
#include "../index_sync/sync_index_timer.h"
#include "../search_util.h"
#include "../process/geo_distance_query_process.h"
#include "log.h"

class CorrectionTest : public testing::Test {
protected:
    static void SetUpTestCase() {
        stat_init_log_("correction_unittest", "../log");
        stat_set_log_level_(7);
        static char conf_filename[1024] = "../conf/index_read.conf";
        if (!SearchConf::Instance()->ParseConf(conf_filename)) {
            printf("parse conf file error: %s.\n", conf_filename);
        }
        SGlobalConfig &global_cfg = SearchConf::Instance()->GetGlobalConfig();
        if (!SplitManager::Instance()->Init(global_cfg.sWordsPath, global_cfg.sTrainPath, global_cfg.sSplitMode)) {
            printf("SplitManager init error\n");
        }

        if (!DataManager::Instance()->Init(global_cfg)){
            printf("DataManager init error\n");
        }

        correction_ = new Correction();
        correction_->Init(global_cfg.sEnWordsPath, global_cfg.sCharacterPath);
    }
    static void TearDownTestCase() {
        if(NULL != correction_){
            delete correction_;
        }
        SearchConf::Instance()->Destroy();
        DataManager::Instance()->Destroy();
        SplitManager::Instance()->Destroy();
    }

    static Correction* correction_;
};

Correction* CorrectionTest::correction_ = NULL;

TEST_F(CorrectionTest, CHINESE_) {
    bool is_correct = false;
    string probably_word;
    correction_->JudgeWord(10001, "京东", is_correct, probably_word);
    EXPECT_TRUE(is_correct);
    EXPECT_EQ("", probably_word);
    correction_->JudgeWord(10001, "京栋", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("京东", probably_word);
    correction_->JudgeWord(10001, "荒鸟", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("荒岛", probably_word);
}

TEST_F(CorrectionTest, HYBRID_) {
    bool is_correct = false;
    string probably_word;
    correction_->JudgeWord(10001, "jing东", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("京东", probably_word);
    correction_->JudgeWord(10001, "京dong", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("京东", probably_word);
}

TEST_F(CorrectionTest, PHONETIC_) {
    bool is_correct = false;
    string probably_word;
    correction_->JudgeWord(10001, "jingdong", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("京东", probably_word);
}

TEST_F(CorrectionTest, ENGLISH_) {
    bool is_correct = false;
    string probably_word;
    correction_->JudgeWord(10001, "apple", is_correct, probably_word);
    EXPECT_TRUE(is_correct);
    EXPECT_EQ("", probably_word);
    correction_->JudgeWord(10001, "applle", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("apple", probably_word);
    correction_->JudgeWord(10001, "appla", is_correct, probably_word);
    EXPECT_FALSE(is_correct);
    EXPECT_EQ("applaud", probably_word);
}

TEST_F(CorrectionTest, DataManager_Sensitive_) {
    bool ret = false;
    ret = DataManager::Instance()->IsSensitiveWord("兴奋剂");
    EXPECT_TRUE(ret);
    ret = DataManager::Instance()->IsSensitiveWord("京东");
    EXPECT_FALSE(ret);
}

TEST_F(CorrectionTest, DataManager_Relate_) {
    bool ret = false;
    vector<string> word_vec;
    ret = DataManager::Instance()->GetRelateWord("apple", word_vec, 1);
    EXPECT_TRUE(ret);
    EXPECT_EQ(1, word_vec.size());
    word_vec.clear();
    ret = DataManager::Instance()->GetRelateWord("banana", word_vec, 1);
    EXPECT_FALSE(ret);
    vector<vector<string> > relate_vvec;
    vector<string> relate_vec;
    relate_vec.push_back("pear");
    relate_vec.push_back("梨");
    relate_vvec.push_back(relate_vec);
    int i_ret = DataManager::Instance()->UpdateRelateWord(relate_vvec);
    EXPECT_EQ(0, i_ret);
}

TEST_F(CorrectionTest, DataManager_Synonym_) {
    vector<FieldInfo> keys;
    DataManager::Instance()->GetSynonymByKey("京东白条", keys);
    EXPECT_EQ(1, keys.size());
    keys.clear();
    DataManager::Instance()->GetSynonymByKey("金条", keys);
    EXPECT_EQ(0, keys.size());
}

TEST_F(CorrectionTest, DataManager_Word_) {
    uint32_t id;
    int ret = DataManager::Instance()->GetWordId("京东", 0, id);
    EXPECT_EQ(0, ret);
    EXPECT_EQ(7754420, id);
    bool b_ret = DataManager::Instance()->IsChineseWord(10001, "京东");
    EXPECT_TRUE(b_ret);
}

TEST_F(CorrectionTest, DataManager_Hot_) {
    bool ret = false;
    vector<string> word_vec;
    ret = DataManager::Instance()->GetHotWord(10001, word_vec, 5);
    EXPECT_TRUE(ret);
    EXPECT_EQ(3, word_vec.size());
    map<uint32_t, map<string, uint32_t> > new_hot_map;
    map<string, uint32_t> word_map;
    word_map["apple"] = 1;
    word_map["pear"] = 3;
    word_map["banana"] = 2;
    new_hot_map[10001] = word_map;
    int i_ret = DataManager::Instance()->UpdateHotWord(new_hot_map);
    EXPECT_EQ(0, i_ret);
}

TEST_F(CorrectionTest, Search_Util_){
    set<vector<Content> > cvset;
    int ret = GetMultipleWords("京东jingr", cvset);
    EXPECT_EQ(0, ret);
    EXPECT_EQ(1, cvset.size());
    cvset.clear();
    ret = GetMultipleWords("jingrong", cvset);
    EXPECT_EQ(0, ret);
    EXPECT_EQ(1, cvset.size());
    cvset.clear();
    ret = GetMultipleWords("bian", cvset);
    EXPECT_EQ(0, ret);
    EXPECT_EQ(2, cvset.size());

    vector<int> i_vec = splitInt("1,2,3", ",");
    EXPECT_EQ(3, i_vec.size());
    set<string> s_set = splitStr("a,b,c", ",");
    EXPECT_EQ(3, s_set.size());

    set<string> v1 = {"a", "b"};
    set<string> v2 = {"b", "c"};
    set<string> intersect_res = sets_intersection(v1, v2);
    EXPECT_EQ(1, intersect_res.size());
    set<string> union_res = sets_union(v1, v2);
    EXPECT_EQ(3, union_res.size());
    set<string> difference_res = sets_difference(v1, v2);
    EXPECT_EQ(1, difference_res.size());

    IntelligentInfo info;
    int len = 0;
    ConvertCharIntelligent("apple", info, len);
    EXPECT_EQ(5, len);

    bool b_ret = isAllNumber("123");
    EXPECT_TRUE(b_ret);

    b_ret = isContainCharacter("京东");
    EXPECT_TRUE(b_ret);

    vector<string> gis_code;
    b_ret = GetGisCode("116.50", "39.76", "", 2, gis_code);
    EXPECT_TRUE(b_ret);
    EXPECT_NE(0, gis_code.size());
    gis_code.clear();
    vector<string> lng_arr = {"121.437271", "121.438022", "121.435297", "121.434524"};
    vector<string> lat_arr = {"31.339747", "31.337291", "31.336814", "31.339252"};
    b_ret = GetGisCode(lng_arr, lat_arr, gis_code);
    EXPECT_TRUE(b_ret);
    EXPECT_NE(0, gis_code.size());

    hash_string_map doc_content;
    hash_double_map distances;
    doc_content["123"] = "{\"geopointTest1\":\"39.452, -76.589\"}";
    GeoPointContext geo_point;
    geo_point.sLatitude = "39.4";
    geo_point.sLongtitude = "76.5";
    b_ret = GetGisDistance(10008, geo_point, doc_content, distances);
    EXPECT_TRUE(b_ret);
}